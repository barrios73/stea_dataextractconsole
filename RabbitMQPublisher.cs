﻿using DataExtractionService.Model;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace DataExtractionService
{
    public class RabbitMQPublisher
    {
        private readonly ILogger<RabbitMQPublisher> _logger;

        public RabbitMQPublisher() { }

        public RabbitMQPublisher(ILogger<RabbitMQPublisher> logger)
        {
            _logger = logger;
        }

        public void Publish(MachineData result, string exchange, string routingKey)
        {
            _logger.LogInformation(result.WOId);
            string rabbitMQUri = "amqp://schan:insituquality@localhost:5672/";
            //string rabbitMQUri = Environment.GetEnvironmentVariable("RABBITMQ_URI");
            var factory = new ConnectionFactory { Uri = new Uri(rabbitMQUri) };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {

                    channel.ExchangeDeclare(exchange: exchange,
                            type: ExchangeType.Direct,
                            durable: true,
                            autoDelete: false);

                    byte[] utfBytes = JsonSerializer.SerializeToUtf8Bytes(result);

                    channel.BasicPublish(exchange: exchange,
                                         routingKey: routingKey,
                                         mandatory: true,
                                         basicProperties: null,
                                         body: utfBytes);
                }
            }
          
        }
    }
}
