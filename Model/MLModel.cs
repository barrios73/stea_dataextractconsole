﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExtractionService.Model
{
    public class MLModel
    {
        public string MachineId { get; set; }
        public string TopicName { get; set; }
        public string InputParam { get; set; }
    }
}
