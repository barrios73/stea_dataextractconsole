﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExtractionService.Model
{
    public class InputParam
    {
        public string inputParamName { get; set; }
        public string inputParamDesc { get; set; }
    }
}
