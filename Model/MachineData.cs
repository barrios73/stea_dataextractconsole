﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExtractionService.Model
{
    public class MachineData
    {
        public string Topic { get; set; }
        public string[] InputParamNames { get; set; }
        public double[][] Data { get; set; }
        public string MachineId { get; set; }
        public string ProductId { get; set; }
        public string ProcessId { get; set; }
        public string WOId { get; set; }
        public string WorkpieceId { get; set; }
        public string TotalQty { get; set; }
    }
}
