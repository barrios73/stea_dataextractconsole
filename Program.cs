﻿// See https://aka.ms/new-console-template for more information

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Hosting.WindowsServices;
using Microsoft.Extensions.DependencyInjection;
using DataExtractionConsole;
using DataExtractionService;

HostApplicationBuilder builder = Host.CreateApplicationBuilder(args);
builder.Services.AddHttpClient();
builder.Services.AddTransient<RabbitMQPublisher>();
builder.Services.AddTransient<DataExtract>();
builder.Services.AddWindowsService(options =>
    options.ServiceName = "PQM DataExtraction Svc Console"
);

using IHost host = builder.Build();



//string path = @"C:\DataMining\ST Aerospace\Latest Info\PlatingData_AutoExtraction";
string path = @"C:\Report";
//string path = "/files";0
FileSystemWatcher fileSystemWatcher = new FileSystemWatcher();


fileSystemWatcher.NotifyFilter = NotifyFilters.LastWrite;
fileSystemWatcher.Path = path;
fileSystemWatcher.Filter = "*Rectifier chrome plate*.csv";
fileSystemWatcher.IncludeSubdirectories = true;
fileSystemWatcher.Changed += FileSystemWatcher_Changed;
fileSystemWatcher.EnableRaisingEvents = true;
Console.ReadLine();

void FileSystemWatcher_Changed(object sender, FileSystemEventArgs e)
{    
    string filepath = e.FullPath;
    string filename = e.Name;
    var app = host.Services.GetRequiredService<DataExtract>();
    app.Extract(filepath, filename);
}



host.Run();