﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataExtractionService.Model
{
    public class ProcessMachine
    {
        public int id { get; set; }
        public string machineId { get; set; }
        public int moduleId { get; set; }
        public int productionLineId { get; set; }
    }
}
