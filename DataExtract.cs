﻿using CsvHelper;
using CsvHelper.Configuration;
using DataExtractionService;
using DataExtractionService.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Formats.Asn1;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace DataExtractionConsole
{
    public class DataExtract
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ILogger<DataExtract> _logger;
        private readonly RabbitMQPublisher _publisher;

        public DataExtract(
            RabbitMQPublisher publisher, IHttpClientFactory httpClientFactory,
            ILogger<DataExtract> logger)
        {
            _publisher = publisher;
            _httpClientFactory = httpClientFactory;
            _logger = logger;
        }



        public async Task Extract(string filepath, string filename)
        {

            _logger.LogInformation("Detected!!!!");


            FinalData fd = ReadLastRowFromCSV(filepath);

            List<string> jobPartList = CreateJobPartList(fd);
            _logger.LogInformation("After CreateJobPartList");

            ProcessMachine selectedMachine = await GetMachineId(filename.ToUpper());
            _logger.LogInformation(selectedMachine.machineId);

            List<MLModel> mlmodelList = await GetMLModelsByMachine(selectedMachine.id);

            foreach (string item in jobPartList)
            {
                foreach (MLModel model in mlmodelList)
                {
                    List<InputParam> inputParamList = JsonSerializer.Deserialize<List<InputParam>>(model.InputParam);

                    MachineData machinedata = new MachineData();
                    machinedata.Topic = model.TopicName;
                    machinedata.InputParamNames = Array.Empty<string>();
                    machinedata.Data = new double[inputParamList.Count][];

                    for (int k = 0; k < inputParamList.Count; k++)
                    {
                        foreach (var prop in fd.GetType().GetProperties(System.Reflection.BindingFlags.DeclaredOnly | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance))
                        {
                            if (prop.Name == inputParamList[k].inputParamName)
                            {
                                machinedata.InputParamNames = machinedata.InputParamNames.Append(prop.Name).ToArray();
                                double[] value = new double[1];
                                value[0] = double.Parse(prop.GetValue(fd).ToString());
                                machinedata.Data[k] = value;
                            }
                        }
                    }

                    machinedata.MachineId = selectedMachine.machineId;
                    machinedata.ProductId = ""; //csv part no is empty so set to "" here
                    machinedata.ProcessId = "1";
                    machinedata.WOId = item;    //job Id
                    machinedata.WorkpieceId = item; //job Id
                    machinedata.TotalQty = "1";


                    //_logger.LogInformation("Before publish");
                    _publisher.Publish(machinedata, "machine", "machine");
                }
            }


        }


        private static FinalData ReadLastRowFromCSV(string filename)
        {
            Thread.Sleep(6000);

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                HasHeaderRecord = true,
                MissingFieldFound = null,
                IgnoreBlankLines = false
            };

            //the filename here can ba passed in from file watcher
            using (var reader = new StreamReader(filename))
            using (var csv = new CsvReader(reader, config))
            {
                csv.Context.RegisterClassMap<DataCSVMap>();
                DataCSV f = new DataCSV();
                //var records = csv.EnumerateRecords(f);

                List<DataCSV> records = csv.GetRecords<DataCSV>().ToList();
                DataCSV lastRecord = records.Last<DataCSV>();


                
                FinalData fd = new FinalData();

                //stop extracting date time
                //fd.DataInTime = DateTime.Parse(lastRecord.DataInTime);

                if (filename.Contains("plate29")) { fd.ChromeTank = "29"; }
                else if (filename.Contains("plate30")) { fd.ChromeTank = "30"; }

                fd.ReadTimer = Double.Parse(lastRecord.ReadTimer);
                fd.RectifierAmpValue = ConvertToDouble(lastRecord.RectifierAmpValue);
                fd.RectifierVoltsValue = ConvertToDouble(lastRecord.RectifierVoltsValue);
                fd.Ramps = ConvertToDouble(lastRecord.Ramps);
                fd.TemperatureChromePlate = ConvertToDouble(lastRecord.TemperatureChromePlate);
                fd.PercentRippleChromePlating = ConvertToDouble(lastRecord.PercentRippleChromePlating);
                fd.Kilowatt = ConvertToDouble(lastRecord.Kilowatt);
                fd.AmpereHour = ConvertToDouble(lastRecord.AmpereHour);
                fd.Part1 = lastRecord.Part1;
                fd.Part2 = lastRecord.Part2;
                fd.Part3 = lastRecord.Part3;
                fd.Part4 = lastRecord.Part4;
                fd.Part5 = lastRecord.Part5;
                fd.Part6 = lastRecord.Part6;
                fd.Job1 = lastRecord.Job1;
                fd.Job2 = lastRecord.Job2;
                fd.Job3 = lastRecord.Job3;
                fd.Job4 = lastRecord.Job4;
                fd.Job5 = lastRecord.Job5;
                fd.Job6 = lastRecord.Job6;

                return fd;
            }

        }


        private static Double? ConvertToDouble(string input)
        {
            Double? output;

            if (input == null || input == "")
            {
                output = null;
            }
            else
            {
                output = Double.Parse(input);
            }

            return output;
        }


        private static void SaveToDb(FinalData data, bool newBatch, bool isFirstRow)
        {
            if (isFirstRow == true)
            {
                Console.WriteLine("GUID \t\t\t\t\t  ReadTimer   \t  Temp    \t  Ramps   \t  PercentRipple   \t" +
                    "RectAmp   \t  RectVolts");
            }

            Console.WriteLine($"{data.Guid} \t {data.ReadTimer.ToString()} \t {data.TemperatureChromePlate} \t" +
                $"{data.Ramps} \t   {data.PercentRippleChromePlating} \t  {data.RectifierAmpValue}    \t" +
                $"{data.RectifierVoltsValue}");
        }


        private static List<string> CreateJobPartList(FinalData fd)
        {
            //Dictionary<string, string> jobPartList = new Dictionary<string, string>();
            List<string> jobPartList = new List<string>();

            if (fd.Job1 != "")
            {
                jobPartList.Add(fd.Job1);   
            }

            if (fd.Job2 != "") 
            {
                jobPartList.Add(fd.Job2);   
            }

            if (fd.Job3 != "")
            {
                jobPartList.Add(fd.Job3);   
            }

            if (fd.Job4 != "") 
            {
                jobPartList.Add(fd.Job4);  
            }

            if (fd.Job5 != "") 
            {
                jobPartList.Add(fd.Job5);   
            }

            if (fd.Job6 != "") 
            {
                jobPartList.Add(fd.Job6);   
            }
            return jobPartList;
        }


        private async Task<ProcessMachine> GetMachineId(string filename)
        {
            try
            {
                ProcessMachine selectedMachine = new ProcessMachine();
                char[] filenameChar = filename.ToCharArray();
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < filenameChar.Length; i++)
                {
                    if (Char.IsLetterOrDigit(filenameChar[i]))
                    {
                        sb.Append(filenameChar[i]);
                    }
                }

                string fileNameOnlyLettersNumber = sb.ToString();

                string baseUrl = "http://localhost:4998";
                string endpoint = "api/Common/GetMachines";
                List<ProcessMachine> machines = await HttpGet<ProcessMachine>(baseUrl, endpoint);

                foreach (ProcessMachine machine in machines)
                {
                    if (fileNameOnlyLettersNumber.Contains(machine.machineId.ToUpper()))
                    {
                        selectedMachine = machine;
                        break;
                    }
                }


                return selectedMachine;
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        private async Task<List<MLModel>> GetMLModelsByMachine(int machineId)
        {

            string baseUrl = "http://localhost:4999";
            string endpoint = "api/MLModel/SearchModelInputParamByMachine";
            string queryparameter = machineId.ToString();

            List<MLModel> modelList = await HttpGet<MLModel>(baseUrl, endpoint, queryparameter);
            return modelList;

        }


        private async Task<List<T>> HttpGet<T>(string baseUrl, string endpoint, string queryparameter = null)
        {
            List<T> result = new List<T>();
            HttpMethod method = HttpMethod.Get;
            Uri uri;

            if (queryparameter == null)
            {
                uri = new Uri(baseUrl + "/" + endpoint);
            }
            else
            {
                uri = new Uri(baseUrl + "/" + endpoint + $"/{queryparameter}");
            }


            var httpRequestMessage = new HttpRequestMessage(method, uri);
            var httpClient = _httpClientFactory.CreateClient();
            
            try
            {
                using HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);


                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    try
                    {
                        string resp = await httpResponseMessage.Content.ReadAsStringAsync();
                        //if (JsonDocument.Parse(resp).RootElement. == JsonArray)

                        var jsonDocument = JsonDocument.Parse(resp);
                        if (jsonDocument.RootElement.ValueKind == JsonValueKind.Object)
                        {
                            string data = jsonDocument.RootElement.GetProperty("data").ToString();
                            result = JsonSerializer.Deserialize<List<T>>(data);
                        }
                        else if (jsonDocument.RootElement.ValueKind == JsonValueKind.Array)
                        {
                            string data = jsonDocument.RootElement.ToString();
                            result = JsonSerializer.Deserialize<List<T>>(data);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.LogInformation(ex.Message);
                    }


                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
            }

            return result;
        }



        public class DataCSV
        {
            //public string DataInTime { get; set; }
            public string RectifierAmpValue { get; set; }       //X6
            public string RectifierVoltsValue { get; set; }
            public string TemperatureChromePlate { get; set; }
            public string Ramps { get; set; }
            public string PercentRippleChromePlating { get; set; }   //X5
            public string AmpereHour { get; set; }
            public string Kilowatt { get; set; }
            public string ReadTimer { get; set; }
            public string Part1 { get; set; }
            public string Part2 { get; set; }
            public string Part3 { get; set; }
            public string Part4 { get; set; }
            public string Part5 { get; set; }
            public string Part6 { get; set; }
            public string Job1 { get; set; }
            public string Job2 { get; set; }
            public string Job3 { get; set; }
            public string Job4 { get; set; }
            public string Job5 { get; set; }
            public string Job6 { get; set; }
        }


        public sealed class DataCSVMap : ClassMap<DataCSV>
        {
            public DataCSVMap()
            {
                //Map(m => m.DataInTime).Name("d/M/yyyy h:mm:ss tt");
                Map(m => m.RectifierAmpValue).Name("Rectifier amps value");
                Map(m => m.RectifierVoltsValue).Name("Rectifier volts value");
                Map(m => m.TemperatureChromePlate).Name("Temperature chrome plate 29", "Temperature chrome plate 30");
                Map(m => m.Ramps).Name("Ramps");
                Map(m => m.PercentRippleChromePlating).Name("% Ripple  Chrome Plating 29", "% Ripple  Chrome Plating 30");
                Map(m => m.AmpereHour).Name("Ampere hour");
                Map(m => m.Kilowatt).Name("Kilowatt");
                Map(m => m.ReadTimer).Name("Read timer");
                Map(m => m.Part1).Name("Part1");
                Map(m => m.Part2).Name("Part2");
                Map(m => m.Part3).Name("Part3");
                Map(m => m.Part4).Name("Part4");
                Map(m => m.Part5).Name("Part5");
                Map(m => m.Part6).Name("Part6");
                Map(m => m.Job1).Name("Job1");
                Map(m => m.Job2).Name("Job2");
                Map(m => m.Job3).Name("Job3");
                Map(m => m.Job4).Name("Job4");
                Map(m => m.Job5).Name("Job5");
                Map(m => m.Job6).Name("Job6");
            }
        }


        public class FinalData
        {
            public string Guid { get; set; }
            //public DateTime DataInTime { get; set; }
            public string ChromeTank { get; set; }
            public double? RectifierAmpValue { get; set; }
            public double? RectifierVoltsValue { get; set; }
            public double? TemperatureChromePlate { get; set; }
            public double? Ramps { get; set; }
            public double? PercentRippleChromePlating { get; set; }
            public double? AmpereHour { get; set; }
            public double? Kilowatt { get; set; }
            public double? ReadTimer { get; set; }
            public string Part1 { get; set; }
            public string Part2 { get; set; }
            public string Part3 { get; set; }
            public string Part4 { get; set; }
            public string Part5 { get; set; }
            public string Part6 { get; set; }
            public string Job1 { get; set; }
            public string Job2 { get; set; }
            public string Job3 { get; set; }
            public string Job4 { get; set; }
            public string Job5 { get; set; }
            public string Job6 { get; set; }
        }


    }
}
